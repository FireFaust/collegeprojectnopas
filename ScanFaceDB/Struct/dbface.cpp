#include "dbface.h"

#include "dbimage.h"

DbFace::DbFace()
{
    dbID = -1;
    imageId = 0;
    x = 0;
    y = 0;
    radius = 0;
}

bool DbFace::save()
{
    QSqlQuery query;
    if(dbID == -1){
        query.prepare("INSERT INTO faces (imageId,x,y,radius,age,gender) "
                      "VALUES (:imageId,:x,:y,:radius,:age,:gender)");
    }else{
        query.prepare("UPDATE images SET "
                      "galleryId = :galleryId, "
                      "x = :x, "
                      "y = :y, "
                      "radius = :radius, "
                      "age = :age, "
                      "gender = :gender "
                      "WHERE id = :id"
                      );
        query.bindValue(":id", dbID);
    }

    query.bindValue(":imageId", imageId);
    query.bindValue(":x", x);
    query.bindValue(":y", y);
    query.bindValue(":radius", radius);
    query.bindValue(":age", age);
    query.bindValue(":gender", gender);

    if(!query.exec()){
        qDebug() << query.lastError().text() << ": "<< query.executedQuery();
        return false;
    }
    if(dbID == -1) dbID = query.lastInsertId().toInt();
    return true;
}

bool DbFace::load(qint64 dbID)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM faces WHERE id = :id");
    query.bindValue(":id", dbID);
    if(query.exec() && query.first()){
        if(!load(query)) return false;
        return true;
    }
    return false;
}


bool DbFace::load(QSqlQuery &query)
{
    this->dbID = query.value("id").toInt();
    this->imageId = query.value("imageId").toInt();
    this->x = query.value("x").toInt();
    this->y = query.value("y").toInt();
    this->radius = query.value("radius").toInt();
    this->age = query.value("age").toDouble();
    this->gender = query.value("gender").toInt();
    return true;
}


DbFace DbFace::get(qint64 dbID)
{
    DbFace obj;
    obj.load(dbID);
    return obj;
}

cv::Mat DbFace::getImg(int radiusCorrection, cv::Mat srcImage)
{
    int radius = this->radius + radiusCorrection;
    if(imageId <= 0 && srcImage.empty()) return cv::Mat();
    cv::Mat imgMat;

    if(srcImage.empty()){
        DbImage img;
        img.load(imageId);
        imgMat = img.getImg();
    }else{
        imgMat = srcImage;
    }

    int shrink = 0;
    if(x < 0) shrink = std::max(shrink,abs(x));
    if(y < 0) shrink = std::max(shrink,abs(y));
    int ti;
    ti = imgMat.size().width - (x+2*radius);
    if(ti < 0) shrink = std::max(shrink,abs(ti));
    ti = imgMat.size().height - (y+2*radius);
    if(ti < 0) shrink = std::max(shrink,abs(ti));

    // Error, face doesnt fit in image
    if(shrink >= radius)  return cv::Mat();

    radius -= shrink;
    cv::Rect rect(x - radius,y - radius,radius*2,radius*2);
    if((x<0)||(y<0))  return cv::Mat();
    cv::Mat result = imgMat(rect).clone();

    return result;
}


bool DbFace::clearInGallery(qint64 galleryId)
{
     QSqlQuery query;
     std::vector<DbFace> result;
     query.prepare("DELETE FROM faces WHERE id in (SELECT f.id AS id FROM faces f, images i  WHERE f.imageId = i.id AND i.galleryId = :galleryId) ");
     query.bindValue(":galleryId", galleryId);

     if(!query.exec()){
         qDebug() << "DbFace::clearInGallery error - " << query.lastError().text() << query.executedQuery() ;
         return false;
     }
     return true;
}

std::vector<DbFace> DbFace::getList(qint64 imageId)
{
    QSqlQuery query;
    std::vector<DbFace> result;
    query.prepare("SELECT * FROM faces WHERE imageId = :imageId ");
    query.bindValue(":imageId", imageId);

    if(!query.exec()){
        qDebug() << "DbFace::getList error - " << query.lastError().text() << query.executedQuery() ;
        return result;
    }

    DbFace tDbFace;

    while (query.next()) {
        tDbFace.dbID = query.value("id").toInt();
        tDbFace.imageId = query.value("imageId").toInt();
        tDbFace.x = query.value("x").toInt();
        tDbFace.y = query.value("y").toInt();
        tDbFace.radius = query.value("radius").toInt();
        tDbFace.age = query.value("age").toDouble();
        tDbFace.gender = query.value("gender").toInt();
        result.push_back(tDbFace);
    }

    return result;
}


std::vector<DbFace> DbFace::getListGallery(qint64 galleryId)
{
    QSqlQuery query;
    std::vector<DbFace> result;
    query.prepare("SELECT f.id AS id, f.imageId AS imageId, f.x AS x, f.y AS y, f.radius AS radius, f.age AS age, f.gender AS gender FROM faces f, images i  WHERE f.imageId = i.id AND i.galleryId = :galleryId ");
    query.bindValue(":galleryId", galleryId);

    if(!query.exec()){
        qDebug() << "DbFace::getList error - " << query.lastError().text() << query.executedQuery() ;
        return result;
    }

    DbFace tDbFace;

    while (query.next()) {
        tDbFace.dbID = query.value("id").toInt();
        tDbFace.imageId = query.value("imageId").toInt();
        tDbFace.x = query.value("x").toInt();
        tDbFace.y = query.value("y").toInt();
        tDbFace.radius = query.value("radius").toInt();
        tDbFace.age = query.value("age").toDouble();
        tDbFace.gender = query.value("gender").toInt();
        result.push_back(tDbFace);
    }

    return result;
}
