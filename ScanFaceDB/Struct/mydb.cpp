#include "mydb.h"

#include "dbimage.h"

MyDb::MyDb(QObject *parent) :
    QObject(parent)
{
    db = QSqlDatabase::addDatabase("QSQLITE");

    QDir initial(DbImage::imageDir);
    if(!initial.mkpath(".")){
        initial = QDir::home();
    }

    db.setDatabaseName( initial.filePath(settings.value("sample_db_filename","samples.db").toString()));
    ok = db.open();

    if(ok) ok = checkTables();

    qDebug() << "DB status - "<<ok;
}



bool MyDb::checkTables()
{
    QSqlQuery q;

    q = db.exec(
                "CREATE TABLE IF NOT EXISTS galleries(" \
                "id                 INTEGER, " \
                "name               TEXT DEFAULT \"\", "\
                "created TIMESTAMP  DEFAULT CURRENT_TIMESTAMP NOT NULL, " \
                "PRIMARY KEY(id ASC));");

    if(!q.isActive()) qDebug() << q.lastError().text();


    q = db.exec(
                "CREATE TABLE IF NOT EXISTS images(" \
                "id                 INTEGER, " \
                "galleryId               INTEGER REFERENCES galleries(id), "\
                "created TIMESTAMP  DEFAULT CURRENT_TIMESTAMP NOT NULL, " \
                "PRIMARY KEY(id ASC));");

    if(!q.isActive()) qDebug() << q.lastError().text();


    q = db.exec(
                "CREATE TABLE IF NOT EXISTS faces(" \
                "id                     INTEGER, " \
                "imageId                INTEGER REFERENCES images(id), "\
                "x                      INTEGER, " \
                "y                      INTEGER, " \
                "radius                 INTEGER, " \
                "age                    REAL, " \
                "gender                 INTEGER, " \
                "PRIMARY KEY(id ASC));");

    if(!q.isActive()) qDebug() << q.lastError().text();
    return true;
}
