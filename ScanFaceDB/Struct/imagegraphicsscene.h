#ifndef IMAGEGRAPHICSSCENE_H
#define IMAGEGRAPHICSSCENE_H

#include <QGraphicsScene>
#include <QPixmap>
#include <QPainter>
#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>
#include <QBitmap>
#include <dbimage.h>
#include <cvhelper.h>

class ImageGraphicsScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit ImageGraphicsScene(QObject *parent = 0);

    void showImage(DbImage image);
    QRect getCurrentView();
    void clear();

signals:
    void faceChanged(int &faceNr);
    void zoomToReq(QRect rect,QString zoomIn_or_Out);
    void takeView();

public slots:
    void showFace(QString position);
    void faceSelected(bool status);
    void changeView(QRect rect);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent);

private:
    QGraphicsPixmapItem * baseItem;
    QPixmap basePixmap;
    DbImage image;
    QRect currentView;

    int faceNr;
    int curFaceNr;

    std::vector<QGraphicsEllipseItem *> faces;
    void setBase(QPixmap pixmap);
};

#endif // IMAGEGRAPHICSSCENE_H
