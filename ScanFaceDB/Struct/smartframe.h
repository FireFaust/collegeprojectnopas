#ifndef SMARTFRAME_H
#define SMARTFRAME_H

#include <QDateTime>

#include "opencv2/imgproc/imgproc.hpp"

class SmartFrame
{
public:
    SmartFrame();

    qint64 timestamp;
    qint64 tLocalizationStart;
    qint64 tLocalizationEnd;
    cv::Mat img;

    std::vector<cv::Rect> faces;
    std::vector<cv::Point> faceCenters;
    std::vector<int> faceSize;
    std::vector<int> faceId;
    std::vector<int> age;
    std::vector<double> agePrecise;
    std::vector<int> gender;
    std::vector<double> ageConf;
    std::vector<double> genderConf;
};

#endif // SMARTFRAME_H
