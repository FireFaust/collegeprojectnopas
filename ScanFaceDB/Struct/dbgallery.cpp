#include "dbgallery.h"

DbGallery::DbGallery()
{
    dbID = -1;
    facesCount = -1;
    imageCount = -1;
    name = "";
}

bool DbGallery::save()
{
    QSqlQuery query;
    if(dbID == -1){
        query.prepare("INSERT INTO galleries (name) "
                      "VALUES (:name)");
    }else{
        query.prepare("UPDATE images SET "
                      "galleryId = :name "
                      "WHERE id = :id"
                      );
        query.bindValue(":id", dbID);
    }

    query.bindValue(":name", name);

    if(!query.exec()){
        qDebug() << query.lastError().text() << ": "<< query.executedQuery();
        return false;
    }

    if(dbID == -1) dbID = query.lastInsertId().toInt();

    return true;
}

bool DbGallery::load(qint64 dbID)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM galleries WHERE id = :id");
    query.bindValue(":id", dbID);
    if(query.exec() && query.first()){
        this->dbID = query.value("id").toInt();
        this->name = query.value("name").toInt();
        return true;
    }

    return false;
}

bool DbGallery::load(QString name)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM galleries WHERE name = :name");
    query.bindValue(":name", name);
    if(query.exec() && query.first()){
        this->dbID = query.value("id").toInt();
        this->name = query.value("name").toString();
        return true;
    }else{
        this->name = name;
        this->save();
    }

    return false;
}

DbGallery DbGallery::get(qint64 dbID)
{
    DbGallery obj;
    obj.load(dbID);
    return obj;
}

std::vector<DbGallery> DbGallery::getList()
{
    QSqlQuery query;
    std::vector<DbGallery> result;
    query.prepare("SELECT a.id AS id, a.name AS name, a.imageCount AS imageCount, CASE WHEN b.facesCount IS NULL THEN 0 ELSE b.facesCount END AS facesCount FROM "
                  "(SELECT g.id AS id, g.name AS name, COUNT(i.id) AS imageCount FROM galleries g, images i WHERE g.id = i.galleryId GROUP BY g.id) a LEFT JOIN "
                  "(SELECT g.id AS id, COUNT(f.id) AS facesCount FROM galleries g, images i, faces f WHERE g.id = i.galleryId AND i.id = f.imageId GROUP BY g.id) b ON (a.id = b.id)");

    if(!query.exec()){
        qDebug() << "DbGallery::getList error - " << query.lastError().text() << query.executedQuery() ;
        return result;
    }

    DbGallery tDbGallery;

    while (query.next()) {
        tDbGallery.dbID = query.value("id").toInt();
        tDbGallery.name = query.value("name").toString();
        tDbGallery.imageCount = query.value("imageCount").toInt();
        tDbGallery.facesCount = query.value("facesCount").toInt();
        result.push_back(tDbGallery);
    }

    return result;
}

QStringList DbGallery::getNameList()
{
    std::vector<DbGallery> objList = getList();
    QStringList result;
    for( std::vector<DbGallery>::iterator it = objList.begin(); it != objList.end(); ++it){
        result.push_back(it->name);
    }
    return result;
}

void DbGallery::loadImages()
{
    images =  DbImage::getList(getDbID());
}

void DbGallery::loadImagesAndFaces()
{
    images = DbImage::getList(getDbID());
    for(std::vector<DbImage>::iterator it = images.begin(); it != images.end(); ++it)
    {
        it->loadFaces();
    }
}

bool DbGallery::deleteAll()
{
    QSqlQuery query;

    if(images.empty())
        images = DbImage::getList(dbID);

    for(std::vector<DbImage>::iterator it = images.begin(); it < images.end(); ++it){
        (*it).deleteAll();
    }

    query.prepare("DELETE FROM galleries WHERE id = :id");
    query.bindValue(":id", dbID);
    if(!query.exec())
        return false;
    return true;
}
