#include "imagegraphicsscene.h"

ImageGraphicsScene::ImageGraphicsScene(QObject *parent) :
    QGraphicsScene(parent)
{
    faceNr = -1;
    baseItem = 0;
}

void ImageGraphicsScene::showImage(DbImage image)
{
    this->image = image;
    setBase(CvHelper::pixmapFrom(image.getImg()));

    for(std::vector<QGraphicsEllipseItem *>::iterator it = faces.begin(); it != faces.end(); ++it){
        this->removeItem(*it);
        delete *it;
    }
    faces.clear();

    QGraphicsEllipseItem * tCircle;
    for(std::vector<DbFace>::iterator it = image.faces.begin(); it != image.faces.end(); ++it)
    {
        tCircle = new QGraphicsEllipseItem(it->x-it->radius, it->y-it->radius,it->radius*2,it->radius*2);
        tCircle->setBrush(QColor(255, 255, 255, 25));
        tCircle->setPen(QPen(QColor(255,0,0,100),4,Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin));
        //tCircle->setFlag(QGraphicsItem::ItemIsSelectable,true);
        faces.push_back(tCircle);
        this->addItem(tCircle);        
    }

}

void ImageGraphicsScene::showFace(QString position)
{
    if(faces.size() > 0){
        if (faceNr == -1) emit takeView();
        if((position == "Next") && (faceNr < int(faces.size()-1)))
                faceNr++;
        else if((position == "Previous") && (faceNr > 0))
                faceNr--;
    }
    if(faceNr != -1){
        QGraphicsEllipseItem * faceItem;
        faceItem = faces.at(faceNr);
        emit zoomToReq(faceItem->rect().toRect(),"Zoom In");
    }
}

void ImageGraphicsScene::faceSelected(bool status)
{
    if(status != true)
        faceNr = -1;
    emit faceChanged(faceNr);
}

QRect ImageGraphicsScene::getCurrentView()
{
    return currentView;
}

void ImageGraphicsScene::changeView(QRect rect)
{
    currentView = rect;
}

void ImageGraphicsScene::setBase(QPixmap pixmap)
{
    basePixmap = pixmap;

    if(baseItem == 0){
        baseItem = this->addPixmap(pixmap);
        this->setSceneRect(0,0,pixmap.width(), pixmap.height());
       // setRect(rect,true);
    }else{
        baseItem->setPixmap(pixmap);
    }
}

void ImageGraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    qDebug() << "mousePressEvent";
}
void ImageGraphicsScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    //qDebug() << "mouseReleaseEvent";
//    if (mouseEvent->button() != Qt::LeftButton)
//        return;
    QGraphicsItem * tItem = itemAt(mouseEvent->scenePos(),QTransform());
    if(tItem == baseItem){
        //qDebug() << "baseItem cliked";
        emit zoomToReq(getCurrentView(),"Zoom Out");
        return;
    }
    bool found = false;
    QGraphicsEllipseItem * faceItem;
    for(std::vector<QGraphicsEllipseItem *>::iterator it = faces.begin(); it != faces.end(); ++it){
        if(*it == tItem){
            if (faceNr == -1) emit takeView();
            faceNr = std::distance(faces.begin(), it);
            faceItem = *it;
            found = true;
            break;
        }
    }

    if(!found){
        qDebug() << "cliked unknown item";
        return;
    }

    emit zoomToReq(faceItem->rect().toRect(),"Zoom In");
    QGraphicsScene::mouseReleaseEvent(mouseEvent);
}

void ImageGraphicsScene::clear()
{
    QGraphicsScene::clear();
    baseItem = 0;
    faceNr = -1;
    for(std::vector<QGraphicsEllipseItem *>::iterator it = faces.begin(); it != faces.end(); ++it){
        this->removeItem(*it);
        delete *it;
    }
    faces.clear();
}
