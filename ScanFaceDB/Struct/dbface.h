#ifndef DBFACE_H
#define DBFACE_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <QVariant>
#include <QDateTime>
#include <QDir>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

class DbFace
{
public:
    static QDir imageDir;

    DbFace();

    qint64 imageId;
    int x;
    int y;
    int radius;
    double age;
    int gender;


    cv::Mat getImg(int radiusCorrection = 0, cv::Mat srcImage = cv::Mat());
    inline cv::Mat getImg(cv::Mat srcImage){return getImg(0,srcImage);}

    bool save();

    bool load(qint64 dbID);
    bool load(QSqlQuery &query);

    inline qint64 getDbID(){return dbID;}
    static DbFace get(qint64 dbID);

    static bool clearInGallery(qint64 galleryId);
    static std::vector<DbFace> getList(qint64 imageId);
    static std::vector<DbFace> getListGallery(qint64 galleryId);

private:

    qint64 dbID;
};

#endif // DBFACE_H
