#include "imagegraphicsview.h"

ImageGraphicsView::ImageGraphicsView(QWidget *parent) :
    QGraphicsView(parent)
{
    setDragMode(ScrollHandDrag);
//    setInteractive(false);
}

void ImageGraphicsView::wheelEvent(QWheelEvent* event) {

    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    double scaleFactor = 1.15;
    if(event->delta() > 0) {
        scale(scaleFactor, scaleFactor);
    } else {
        scale(1.0 / scaleFactor, 1.0 / scaleFactor);
    }
    emit faceSelected(false);
}


void ImageGraphicsView::zoomTo(QRect rect,QString zoomIn_or_Out)
{
    if(zoomIn_or_Out == "Zoom In")
        emit faceSelected(true);
    else
        emit faceSelected(false);
    fitInView(rect, Qt::KeepAspectRatio);
    //qDebug() << "ImageGraphicsView::zoomTo";
}


void ImageGraphicsView::mousePressEvent(QMouseEvent * event)
{
//    QGraphicsView::mousePressEvent(event);
//    qDebug() << "ImageGraphicsView::mousePressEvent";
}
