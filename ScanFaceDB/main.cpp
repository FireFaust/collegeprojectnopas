#include "mainwindow.h"
#include "Proc/dispatcher.h"
#include <openbr/openbr_plugin.h>

int main(int argc, char *argv[])
{
    br::Context::initialize(argc, argv);
    QCoreApplication::setOrganizationName("Applyit");
    QCoreApplication::setOrganizationDomain("applyit.lv");
    QCoreApplication::setApplicationName("ScanFaceDB");

    Dispatcher a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
