#include "optrect.h"

OptRect::OptRect()
{
    setDefaults();
}

OptRect::OptRect(int x, int y, int width, int height)
{
    this->x = x;
    this->y = y;
    this->width = width;
    this->height = height;
}

void OptRect::setDefaults()
{
    x=y=width=height = 0;
}

bool OptRect::setJSON(QJsonValue value)
{
    if(!value.isObject()){
        qWarning("Opt: Invalid type - must be object for rectangle");
        return false;
    }
    QJsonObject opt = value.toObject();
    //=============================================================== x
    if(!getInt(opt,x,"x")) return false;
    //=============================================================== y
    if(!getInt(opt,y,"y")) return false;
    //=============================================================== width
    if(!getInt(opt,width,"width")) return false;
    //=============================================================== height
    if(!getInt(opt,height,"height")) return false;
    return true;
}

QJsonValue OptRect::getJSON()
{
    QJsonObject tObj;
    tObj.insert("x",x);
    tObj.insert("y",y);
    tObj.insert("width",width);
    tObj.insert("height",height);
    return QJsonValue(tObj);
}

bool OptRect::operator==(OptRect &other){
    if(x != other.x) return false;
    if(y != other.y) return false;
    if(width != other.width) return false;
    if(height != other.height) return false;
    return true;
}
