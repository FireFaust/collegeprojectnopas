#ifndef OPTSENDER_H
#define OPTSENDER_H

#include "opt.h"
#include <QString>

class OptSender : public Opt
{
public:
    OptSender();

    bool setJSON(QJsonValue value);
    QJsonValue getJSON();
    void setDefaults();

    QString frameSendUrl;
    QString rectGetUrl;
    int maxFrames;
    int minFrames;
    int maxQueue;

    bool operator==(OptSender &other);
    inline bool operator!=(OptSender &other){return !(*this == other);};
};

#endif // OPTSENDER_H
