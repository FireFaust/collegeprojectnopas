#include "importer.h"

#include "dbgallery.h"
#include "dbimage.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv/cv.h>
#include <QCoreApplication>

Importer::Importer(QObject *parent) :
    QObject(parent),
    importing(false)
{

}

void Importer::doTerminate()
{
    emit finished();
}

void Importer::importDir(QString path, QString galleryName, bool recursive)
{
    if(importing){
        qDebug() << "Cant import (allready importing)";
        return;
    }
    importing = true;
    qDebug() << "Importing directory;";
    DbGallery gallery;
    gallery.load(galleryName);
    cv::Mat frame;
    int ti = 0;
    QStringList list;
    QDirIterator *iterator;
    if(recursive == true)
        iterator = new QDirIterator(path, QDirIterator::Subdirectories);
    else
        iterator = new QDirIterator(path, QDirIterator::NoIteratorFlags);
    while (iterator->hasNext()) {
       iterator->next();
       if (!iterator->fileInfo().isDir()) {
          QString filename = iterator->filePath();
          list.append(filename);
       }
    }
    qSort(list.begin(),list.end(),naturalSortCompare());
    for(int i=0;i<list.size();i++)
    {
        qDebug("Found %s matching pattern.", qPrintable(list.at(i)));
        frame = cv::imread(list.at(i).toStdString());
        DbImage image;
        image.galleryId = gallery.getDbID();
        image.setImg(frame);
        image.save();
        ti++;
        emit progress(-1,ti);
        QCoreApplication::processEvents();
    }

    emit progress(0,0);
    importing = false;
}

void Importer::importVideo(QString path, QString galleryName)
{
    if(importing){
        qDebug() << "Cant import (allready importing)";
        return;
    }
    importing = true;
    qDebug() << "Importing file";
    DbGallery gallery;
    gallery.load(galleryName);

    cv::VideoCapture capture(qPrintable(path));
    cv::Mat frame;

    if(!capture.isOpened()){
        qDebug() << "Error, video not loaded";
        emit progress(0,0);
        importing = false;
        return;
    }

    int ti = 0;
    while(capture.read(frame))
    {
        DbImage image;
        image.galleryId = gallery.getDbID();
        image.setImg(frame);
        image.save();
        ti++;
        emit progress(-1,ti);
        QCoreApplication::processEvents();
        //TODO For development only limit frame count
        //if(ti > 125) break;
    }

    emit progress(0,0);
    importing = false;
}
