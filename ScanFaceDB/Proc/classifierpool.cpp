#include "classifierpool.h"

ClassifierPool::ClassifierPool(QObject *parent) :
    QObject(parent)
{
    threadCount = QThread::idealThreadCount();
    qDebug() << "ClassifierPool::threadCount " << threadCount;
    maxTimingFrames =  2*threadCount;
    mustTerminate = false;
    busyThreads = 0;

    QThread * tThread;
    Classifier * tClassifier;
    for(int i = 0; i < threadCount; i++){
        tThread = new QThread();
        tClassifier = new Classifier();
        classifierThreads.push_back(tThread);
        tClassifier->moveToThread(tThread);
        classifiers.push_back(tClassifier);
        classifierBusy.push_back(false);
        tThread->start();

        connect(tClassifier,&Classifier::frameFaceProcessed,this,&ClassifierPool::frameFaceProcessed);
    }
}

void ClassifierPool::doTerminate()
{
    mustTerminate = true;
    for(int i = 0; i < threadCount; i++){
        if(classifierBusy[i]){
            return;
        }
    }
    emit(finished());
}

void ClassifierPool::processFrame(DbImage frame)
{
    if(mustTerminate) return;

    // All localizers busy
    frameQueue.push_back(frame);

    if(!frame.getImg().empty()){
    localProcessFrame();
    }
//    for(int i = 0; i < threadCount; i++){
//        if(!classifierBusy[i]){
//            localProcessFrame();
//            classifierBusy[i] = true;
//            frame.tLocalizationStart = QDateTime::currentMSecsSinceEpoch();
//            QMetaObject::invokeMethod(classifiers[i], "processFrame", Qt::QueuedConnection, Q_ARG(SmartFrame, frame), Q_ARG(int, i));
//            return;
//        }
//    }
}

void ClassifierPool::localProcessFrame()
{

    if(frameQueue.empty()) return;

    bool testBusy = false;
    int busyThreadsTest = 0;
    for(int i = 0; i < threadCount; i++){

        if(!classifierBusy[i] && !frameQueue.empty()){
            classifierBusy[i] = true;
            QMetaObject::invokeMethod(classifiers[i], "processFrameFace", Qt::QueuedConnection, Q_ARG(DbImage, frameQueue.front()), Q_ARG(int, i));
            frameQueue.pop_front();
        }
        if(classifierBusy[i]) busyThreadsTest ++;
    }
    busyThreads = busyThreadsTest;
}

void ClassifierPool::frameFaceProcessed(DbImage frame, int threadNr)
{


    classifierBusy[threadNr] = false;
    busyThreads --;

    if(mustTerminate){
        bool canTerminate = true;
        for(int i = 0; i < threadCount; i++){
            if(classifierBusy[i]) canTerminate = false;
        }
        if(canTerminate) emit(finished());
        return;
    }

    qDebug() << "ClassifierPool::frameProcessed";

    localProcessFrame();
    frame.save();
    emit frameClassified(frameQueue.size() + busyThreads);
    emit frameProcessed(frame);
}
