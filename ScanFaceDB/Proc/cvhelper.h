#ifndef CVHELPER_H
#define CVHELPER_H


#include <QDebug>
#include <QPixmap>
#include <QImage>

#include "opencv2/imgproc/imgproc.hpp"

class CvHelper
{
public:

    static inline QPixmap pixmapFrom(cv::Mat imgBGR, int maxHeight = -1, int maxWidth = -1)
    {
        cv::Mat img;
        if(imgBGR.channels() == 1){
            cv::cvtColor( scaleMat(imgBGR,maxHeight, maxWidth), img, cv::COLOR_GRAY2RGB);
        }else{
            cv::cvtColor( scaleMat(imgBGR,maxHeight, maxWidth), img, cv::COLOR_BGR2RGB);
        }
        return QPixmap::fromImage(QImage((unsigned char*) img.data, img.cols, img.rows, QImage::Format_RGB888));
    }

    static inline QPixmap pixmapFromRGB(cv::Mat imgRGB, int maxHeight = -1, int maxWidth = -1)
    {
        cv::Mat img = scaleMat(imgRGB,maxHeight, maxWidth);
        return QPixmap::fromImage(QImage((unsigned char*) img.data, img.cols, img.rows, QImage::Format_RGB888));
    }

    static inline cv::Mat scaleMat(cv::Mat img, int maxHeight = -1, int maxWidth = -1, bool forceClone = false)
    {
        if(maxHeight <= 0 && maxWidth <= 0) return forceClone?img.clone():img;
        cv::Size size = img.size();
        double scale;
        if(maxHeight>0){
            scale = (double)maxHeight/size.height ;
            if(scale < 1){
                size.width = (int)(scale * size.width);
                size.height = (int)(scale * size.height);
            }
        }

        if(maxWidth>0){
            scale = (double)maxWidth/size.width ;
            if(scale < 1){
                size.width = (int)(scale * size.width);
                size.height = (int)(scale * size.height);
            }
        }

        if(size.width%4 != 0){
            int nWidth = size.width - size.width%4;
            scale = (double)nWidth/size.width ;
            if(scale < 1){
                size.width = nWidth;
                size.height = (int)(scale * size.height);
            }
        }

        if(size == img.size()) return forceClone?img.clone():img;
        cv::Mat resized;
        cv::resize(img, resized, size, 0, 0, cv::INTER_LINEAR);
        return resized;
    }
};

#endif // CVHELPER_H
