#ifndef GALLERYVIEW_H
#define GALLERYVIEW_H

#include <QWidget>
#include <QVBoxLayout>
#include <QScrollArea>

#include <QDir>
#include <QFileDialog>
#include <iostream>
#include <QSignalMapper>

using namespace std;

#include "dbgallery.h"
#include "imagegraphicsscene.h"
#include "cvhelper.h"

namespace Ui {
class GalleryView;
}

class GalleryView : public QWidget
{
    Q_OBJECT

public:
    explicit GalleryView(QWidget *parent = 0);
    ~GalleryView();
    void loadGallery(qint64 galleryId);
signals:
    void showFace(QString);
    void giveView(QRect);
private slots:
    void btnNextImageClicked();
    void btnPreviousImageClicked();
    void btnNextFaceClicked();
    void btnPreviousFaceClicked();
    void btnOutputDirClicked();
    void btnFolderClicked(QString);
    void btnSaveAllClicked();
    void currentFaceStatus(int&);
    void currentView();
private:
    Ui::GalleryView * ui;

    QScrollArea *scrollArea;
    QWidget *bWidget;
    QVBoxLayout *vbox;
    std::vector<QPushButton*> buttonList;

    QDir outputDir;
    QStringList dirList;
    QString imageName;
    QSignalMapper *signalMapper;

    ImageGraphicsScene * scene;
    DbGallery gallery;
    int currentImageNr;
    int currentFaceNr;


    void toImage(int nr);
    void saveImage(QString &name);
};

#endif // GALLERYVIEW_H
