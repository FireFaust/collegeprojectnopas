#include "galleryview.h"
#include "ui_galleryview.h"

GalleryView::GalleryView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GalleryView)
{
    ui->setupUi(this);

    signalMapper = new QSignalMapper(this);

    bWidget = new QWidget;
    scrollArea = new QScrollArea;
    vbox = new QVBoxLayout;
    scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    vbox->setSizeConstraint(QLayout::SetMinAndMaxSize);

    ui->groupBox->setMinimumHeight(150);
    ui->groupBox->setFixedWidth(178);
    ui->groupBox->layout()->addWidget(scrollArea);

    scene = new ImageGraphicsScene(this);
    ui->imageView->setScene(scene);
    ui->imageView->setDragMode(QGraphicsView::ScrollHandDrag);
//    ui->imageView->setMouseTracking(true);

    connect(ui->btnPreviousImage,&QPushButton::clicked, this, &GalleryView::btnPreviousImageClicked);
    connect(ui->btnNextImage,&QPushButton::clicked, this, &GalleryView::btnNextImageClicked);
    connect(ui->btnNextFace,&QPushButton::clicked,this, &GalleryView::btnNextFaceClicked);
    connect(ui->btnPrevFace,&QPushButton::clicked,this, &GalleryView::btnPreviousFaceClicked);
    connect(ui->btnOutputDir,&QPushButton::clicked, this, &GalleryView::btnOutputDirClicked);
    connect(ui->btnSaveAll,&QPushButton::clicked,this,&GalleryView::btnSaveAllClicked);
    connect(signalMapper,SIGNAL(mapped(QString)),this,SLOT(btnFolderClicked(QString)));
    connect(this,&GalleryView::showFace,scene,&ImageGraphicsScene::showFace);

    connect(ui->imageView,SIGNAL(faceSelected(bool)),scene,SLOT(faceSelected(bool)));
    connect(scene,SIGNAL(faceChanged(int&)),this,SLOT(currentFaceStatus(int&)));
    connect(scene,SIGNAL(takeView()),this,SLOT(currentView()));
    connect(this,SIGNAL(giveView(QRect)),scene,SLOT(changeView(QRect)));

    connect(scene,&ImageGraphicsScene::zoomToReq, ui->imageView, &ImageGraphicsView::zoomTo);
    currentImageNr = 0;
}

GalleryView::~GalleryView()
{
    delete ui;
}


void GalleryView::loadGallery(qint64 galleryId)
{
    gallery.load(galleryId);
    gallery.loadImagesAndFaces();
    currentImageNr = 0;
    currentFaceNr = 0;

    if(gallery.images.empty()) return;

    scene->showImage(gallery.images[currentImageNr]);
}

void GalleryView::btnNextImageClicked()
{
    toImage(currentImageNr+1);
}

void GalleryView::btnPreviousImageClicked()
{
    toImage(currentImageNr-1);
}

void GalleryView::btnNextFaceClicked()
{
    showFace("Next");
}

void GalleryView::btnPreviousFaceClicked()
{
    showFace("Previous");
}

void GalleryView::btnOutputDirClicked()
{    
    outputDir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                  QDir::homePath(),
                                                  QFileDialog::ShowDirsOnly
                                                  | QFileDialog::DontResolveSymlinks);
    if(outputDir.absolutePath() != ""){

        scrollArea->deleteLater();
        vbox->deleteLater();
        bWidget->deleteLater();
        buttonList.clear();

        dirList = outputDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
        ui->label->setText("Directory selected");
        scrollArea = new QScrollArea;
        vbox = new QVBoxLayout;
        bWidget = new QWidget;
        scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

        QPushButton *pb;
        for (QStringList::Iterator i=dirList.begin(); i<dirList.end();i++){
            pb = new QPushButton(QString(*i));
            pb->setFixedWidth(115);
            buttonList.push_back(pb);
            vbox->addWidget(pb);
            connect(pb,SIGNAL(clicked()),signalMapper,SLOT(map()));
            signalMapper->setMapping(pb,pb->text());
        }

        bWidget->setLayout(vbox);
        scrollArea->setWidget(bWidget);
        ui->groupBox->layout()->addWidget(scrollArea);
    }
}

void GalleryView::btnFolderClicked(QString button)
{
    if(currentFaceNr < 0) {
        ui->label_2->setText("Object not selected");
    }
    else{
        int imageNumber = gallery.images[currentImageNr].faces[currentFaceNr].imageId;
        ui->label_2->setText("Object saved");
        imageName = outputDir.absolutePath()+"/"+button+"/"+QString::number(imageNumber)+"_"+\
                QString::number(currentFaceNr)+".jpg";
        saveImage(imageName);
    }
}

void GalleryView::btnSaveAllClicked()
{
    std::vector<int> qualityType;
    qualityType.push_back(CV_IMWRITE_JPEG_QUALITY);
    qualityType.push_back(99);

    if (outputDir.absolutePath()!="")
    {
        vector<DbGallery> galleryList = DbGallery::getList();
        for(vector<DbGallery>::iterator it = galleryList.begin(); it < galleryList.end(); ++it){

            vector<DbImage> imageList = DbImage::getList((*it).getDbID());
            for(vector<DbImage>::iterator iit = imageList.begin(); iit < imageList.end(); ++iit){
                cv::Mat image = (*iit).getImg();
                vector<DbFace> faceList = DbFace::getList((*iit).getDbID());
                for(vector<DbFace>::iterator iitt = faceList.begin(); iitt < faceList.end(); ++iitt){
                    cv::imwrite(QString(outputDir.absolutePath()+"/%1.jpg").arg((*iitt).getDbID()).toStdString(), (*iitt).getImg(0, image));
                }
            }
        }
    }
    qDebug() << "no output directory specified";
}

void GalleryView::currentFaceStatus(int &faceNr)
{
    currentFaceNr = faceNr;
    if((currentFaceNr == -1)){
        ui->label_2->setText("");
        ui->label_3->setText("");
    }
    else{
        ui->label_2->setText("Object selected");
        QString faceInfo;
        faceInfo.append("Size: "+QString::number(gallery.images[currentImageNr].faces[currentFaceNr].radius*2)+\
                        " x "+QString::number(gallery.images[currentImageNr].faces[currentFaceNr].radius*2)+" px\n");

        if(gallery.images[currentImageNr].faces[currentFaceNr].gender == 0)
            faceInfo.append("Male");
        else
            faceInfo.append("Female");
        faceInfo.append(" | "+QString::number(gallery.images[currentImageNr].faces[currentFaceNr].age));
        ui->label_3->setText(faceInfo);
    }
}

void GalleryView::currentView()
{
    QRect visibleRect = ui->imageView->mapToScene(ui->imageView->rect()).boundingRect().toRect();
    emit giveView(visibleRect);
}

void GalleryView::saveImage(QString &name)
{
    std::vector<int> qualityType;
    qualityType.push_back(CV_IMWRITE_JPEG_QUALITY);
    qualityType.push_back(99);
    cv::imwrite(name.toStdString(),gallery.images[currentImageNr].faces[currentFaceNr]\
               .getImg(0,gallery.images[currentImageNr].getImg()),qualityType);
}

void GalleryView::toImage(int nr)
{
    if(gallery.images.empty()) return;
    if(nr < 0) nr  = gallery.images.size();
    if(nr >= int(gallery.images.size())) nr = 0;
    currentImageNr = nr;
    currentFaceNr = -1;

    scene->showImage(gallery.images[currentImageNr]);
    ui->label_2->setText("");
    ui->label_3->setText("");
    ui->label_4->setText("Image "+QString::number(currentImageNr)+" of "+QString::number(gallery.images.size()));
}
