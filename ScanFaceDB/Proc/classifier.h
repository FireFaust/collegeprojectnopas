#ifndef CLASSIFIER_H
#define CLASSIFIER_H

#include <QObject>
#include <QFile>
#include <QDir>
#include <QFileInfo>
#include <dbimage.h>
#include <openbr/openbr_plugin.h>

class Classifier : public QObject
{
    Q_OBJECT
public:
    explicit Classifier(QObject *parent = 0);

signals:
    void frameFaceProcessed(DbImage frame, int threadNr);

public slots:
    void processFrameFace(DbImage frame, int threadNr);

private:
    QSharedPointer<br::Transform> transformG;
    QSharedPointer<br::Transform> transformA;
};

#endif // CLASSIFIER_H
