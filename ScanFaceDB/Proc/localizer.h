#ifndef LOCALIZER_H
#define LOCALIZER_H

#include <QObject>
#include <QDebug>
#include <QFile>
#include <QDir>
#include "dbimage.h"

#include "opencv2/objdetect/objdetect.hpp"
#include "settings/smartsettings.h"
#include "parsedfaces.h"
#include "Struct/dbface.h"
#include "structs.h"

class ParseCascade
{
public:
    int flags;
    cv::Size minSize;
    cv::Size maxSize;
    bool hasMaxSize;
    QString path;
    double scaleStep;
    int minNeighbors;
    cv::CascadeClassifier classifier;

    inline bool fromOpt(OptCascade tCascade){
        if(!classifier.load(tCascade.path.toStdString()))
        {
            qWarning() << "Cascade file "<< tCascade.path <<" not found";
            return false;
        }
        flags = tCascade.flags;
        hasMaxSize = tCascade.hasMaxSize;
        maxSize = tCascade.maxSize;
        minNeighbors = tCascade.minNeighbors;
        minSize = tCascade.minSize;
        path = tCascade.path;
        scaleStep = tCascade.scaleStep;
        return true;
    }
};

class ParseRotation
{
public:
    int maxAngle;
    int angleStep;
    std::vector<ParseCascade> cascades;
};

class Localizer : public QObject
{
    Q_OBJECT
public:
    explicit Localizer(QObject *parent = 0);

signals:
    void frameProcessed(DbImage frame, int threadNr);

public slots:
    void processFrame(DbImage frame, int threadNr);
    void setClassifier(int type);
private:

    int type;
    void init();
    SmartSettings settings;

    cv::Mat greyImg;
    cv::CascadeClassifier cascade;
    std::vector<cv::Rect> tFaces;
    std::vector<cv::Rect> tFacesFiltered;

    // Resulting faces
    cv::Mat *flip_mat; //!< cv::Mat for frame rotation 90 degrees;
    cv::Size flip_mat_size;
    cv::Point flip_mat_center;
    cv::Mat *rot_mat; //!< cv::Mat for frame rotation;
    cv::Size rot_mat_size;
    cv::Scalar flip_rot_mat_def_contents;
    cv::Point rot_mat_center;

    ParsedFaces faces;

    //void InitClassifiers();
    void RotateFrame(cv::Mat src, cv::Mat &dst, double angle);
    cv::Rect ContainedRotatedRect(cv::Rect source, cv::Point center, double angle);
    cv::Point RotatePoint(cv::Point center, cv::Point point, double angle);

    bool initSettings(SmartSettings settings);
    std::vector<ParseRotation> rotations;
    std::vector<ParseCascade> cascadesPostproces;

    bool debug;
};

#endif // LOCALIZER_H
