#ifndef LOCALIZERPOOL_H
#define LOCALIZERPOOL_H

#include <QObject>
#include <QThread>
#include <QDebug>
#include <deque>
#include <vector>

#include "smartframe.h"
#include "localizer.h"
#include "dbimage.h"

class LocalizerPool : public QObject
{
    Q_OBJECT
public:
    explicit LocalizerPool(QObject *parent = 0);

signals:
    void finished();
    void frameProcessed(DbImage frame);
    void progress(int curr, int total);

public slots:
    void doTerminate();
//    void frameGrabbed(SmartFrame frame);
    void processGallery(qint64 galleryId);
    void frameClassified(int queue);
    void setClassifier(int cascadeType);

private slots:
    void frameProcessedLocaly(DbImage frame, int threadNr);

private:
    std::deque<SmartFrame> frameQueue;

    std::vector<QThread *> localizerThreads;
    std::vector<Localizer *> localizers;
    std::vector<bool> localizerBusy;
    std::deque<qint64> timing;
    qint64 lastFrameProcessed;

    std::vector<DbImage> imageList;
    qint64 currImageInList;
    qint64 currImagesProcessing;

    qint64 lastFrameTimestamp;

    int threadCount;
    int maxQueue;
    int maxTimingFrames;

    bool mustTerminate;
    void processFrames();
};

#endif // LOCALIZERPOOL_H
