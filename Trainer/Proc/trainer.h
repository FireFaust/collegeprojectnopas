#ifndef TRAINER_H
#define TRAINER_H

#include <QObject>

#include <QProcess>
#include <QSettings>
#include <QDateTime>
#include <QFile>
#include <QThread>

#include <QDebug>

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <ctime>


#include "cascadeclassifier.h"
#include "utility.hpp"

using namespace std;
using namespace cv;

class Trainer : public QObject
{
    Q_OBJECT
public:
    explicit Trainer(QObject *parent = 0);
    ~Trainer();
    QString vecName;
private:
    QSettings *settings;
signals:

public slots:
    bool doSample(QString type);
    void doTraining();
};

#endif // TRAINER_H
