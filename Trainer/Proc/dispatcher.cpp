#include "dispatcher.h"
#include "ui_dispatcher.h"

Dispatcher::Dispatcher(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Dispatcher)
{
    ui->setupUi(this);
    QDir *dir;
    dir = new QDir("Vectors");
    dir->mkpath(dir->absolutePath());
    dir = new QDir("data");
    dir->mkpath(dir->absolutePath());
    dir = new QDir("positive");
    dir->mkpath(dir->absolutePath());
    dir = new QDir("negative");
    dir->mkpath(dir->absolutePath());
    delete dir;

    dirA          = new DirectoryAnalyzer;
    settingsField = new SettingsField;
    trainer       = new Trainer;

    drawFields();
}

Dispatcher::~Dispatcher()
{
    delete ui;
    system("pkill opencv_traincascade");
}

void Dispatcher::drawFields()
{
    mainLayout = new QGridLayout;

    QLabel *positiveImages     = new QLabel("Positive Images");
    QLabel *negativeImages     = new QLabel("Negative Images");
    QLabel *prepareImages      = new QLabel("Prepare Images");
    QLabel *startTraining      = new QLabel("startTraining");  

    posButton      = new QPushButton("select");
    negButton      = new QPushButton("select");
    prepareButton  = new QPushButton("prepare");
    createSamples  = new QPushButton("create");
    trainingButton = new QPushButton("training");
    sampleBox      = new QComboBox;

    sampleBox->addItem("Image set");
    sampleBox->addItem("Image object");

    QHBoxLayout *posLayout     = new QHBoxLayout;
    QHBoxLayout *negLayout     = new QHBoxLayout;
    QHBoxLayout *prepLayout    = new QHBoxLayout;
    QHBoxLayout *createLayout  = new QHBoxLayout;
    QHBoxLayout *startLayout   = new QHBoxLayout;

    posLayout->addWidget(positiveImages);
    posLayout->addWidget(posButton);

    negLayout->addWidget(negativeImages);
    negLayout->addWidget(negButton);

    prepLayout->addWidget(prepareImages);
    prepLayout->addWidget(prepareButton);

    createLayout->addWidget(sampleBox);
    createLayout->addWidget(createSamples);

    startLayout->addWidget(startTraining);
    startLayout->addWidget(trainingButton);

    QGroupBox *pos    = new QGroupBox;
    QGroupBox *neg    = new QGroupBox;
    QGroupBox *prep   = new QGroupBox;
    QGroupBox *create = new QGroupBox;
    QGroupBox *start  = new QGroupBox;

    pos->setLayout(posLayout);
    neg->setLayout(negLayout);
    prep->setLayout(prepLayout);
    create->setLayout(createLayout);
    start->setLayout(startLayout);

    mainLayout->addWidget(pos,0,1);
    mainLayout->addWidget(neg,1,1);
    mainLayout->addWidget(prep,2,1);
    mainLayout->addWidget(create,3,1);
    mainLayout->addWidget(start,4,1);

    ui->centralwidget->setLayout(mainLayout);
    ui->centralwidget->setMaximumHeight(768);

    connect(posButton,    SIGNAL(clicked()),this,SLOT(searchPositive()));
    connect(negButton,    SIGNAL(clicked()),this,SLOT(searchNegative()));
    connect(prepareButton,SIGNAL(clicked()),this,SLOT(prepare()));
    connect(ui->actionSettings,SIGNAL(triggered()),this,SLOT(setSettings()));
    connect(ui->actionViewSettings,SIGNAL(triggered()),this,SLOT(showSettings()));
    connect(createSamples,SIGNAL(clicked()),this,SLOT(startSample()));
    connect(trainingButton,SIGNAL(clicked()),trainer,SLOT(doTraining()));
    connect(sampleBox,SIGNAL(activated(QString)),this,SLOT(typeChanged(QString)));
    //connect(trainingButton,SIGNAL(clicked()),trainer,SLOT(deprecated_doTraining()));
}

void Dispatcher::searchPositive()
{
    posDir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                            QDir::homePath(),
                                                            QFileDialog::ShowDirsOnly
                                                            | QFileDialog::DontResolveSymlinks);

    positiveList = dirA->findFile("*.jpg",posDir,true);
}

void Dispatcher::searchNegative()
{
    negDir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                            QDir::homePath(),
                                                            QFileDialog::ShowDirsOnly
                                                            | QFileDialog::DontResolveSymlinks);

    negativeList = dirA->findFile("*.jpg",negDir,true);
}

void Dispatcher::prepare()
{
    qDebug() <<QDateTime::currentDateTime().toString("hh:mm:ss:zzz").toStdString().c_str() <<"trying to prepare";

    if((positiveList.size()==0)&&(negativeList.size()==0))
    {
        qWarning() <<QDateTime::currentDateTime().toString("hh:mm:ss:zzz").toStdString().c_str()<< "nothing to prepare";
        return;
    }

    if((positiveList.size()>0)&&(negativeList.size()==0))
    {
        qWarning() <<QDateTime::currentDateTime().toString("hh:mm:ss:zzz").toStdString().c_str()<< "no negative images" << "preparing only positive data set";
    }
    if((positiveList.size()==0)&&(negativeList.size()>=0))
    {
        qWarning() <<QDateTime::currentDateTime().toString("hh:mm:ss:zzz").toStdString().c_str()<< "no positive images" << "preparing only positive data set";
    }

    QString tStr = "_"+QDateTime::currentDateTime().toString("yyyy_dd_MM_hh:mm:ss:zzz");
    QFile positive("positive/positive"+tStr+".dat");
    QFile negative("negative/negative"+tStr+".dat");

    positive.open(QIODevice::WriteOnly);
    negative.open(QIODevice::WriteOnly);

    Mat image;

    QTextStream *pStream = new QTextStream(&positive);
    QTextStream *nStream = new QTextStream(&negative);

    for (int i=0;i<positiveList.size();i++)
    {
        image = imread(positiveList.at(i).toStdString());
        *pStream << positiveList.at(i)<< " 1 " << "0 " << "0 " << QString::number(image.cols) <<" "<< QString::number(image.rows)<<endl;
    }

    for (int i=0;i<negativeList.size();i++)
    {
        *nStream << negativeList.at(i)<< endl;
    }
    if(positiveList.size()>0)
    {
        QDir dir("positive/positive"+tStr+".dat");
        settingsField->settings->settings.setValue("info",dir.absolutePath());
        settingsField->settings->settings.setValue("numPos",positiveList.size());
        settingsField->settings->settings.setValue("num",positiveList.size());
        settingsField->maxPos = positiveList.size();
        settingsField->settings->setNewSettings(settingsField->path);
    }
    if(negative.size()>0)
    {
        QDir dir("negative/negative"+tStr+".dat");
        settingsField->settings->settings.setValue("bg",dir.absolutePath());
        settingsField->settings->settings.setValue("numNeg",negativeList.size());
        settingsField->maxNeg = negativeList.size();
        settingsField->settings->setNewSettings(settingsField->path);
    }
    qWarning() << QDateTime::currentDateTime().toString("hh:mm:ss:zzz").toStdString().c_str() << "Prepare Complited";
    settingsField->doUpdate();
}

void Dispatcher::setSettings()
{
    settingsField->show();
}

void Dispatcher::showSettings()
{
    settingsField->showSettings();
}

void Dispatcher::startSample()
{    
    if(trainer->doSample(sampleBox->currentText()))
    {
        settingsField->settings->settings.setValue("vec",trainer->vecName);
        settingsField->settings->setNewSettings(settingsField->path);
    }
    //trainer->deprecated_doSample(sampleBox->currentText());
}

void Dispatcher::typeChanged(QString value)
{

    if(value=="Image object")
        settingsField->singleImage = true;
    else
        settingsField->singleImage = false;
    settingsField->doUpdate();
}
