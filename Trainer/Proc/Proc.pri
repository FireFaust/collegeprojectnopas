INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD

HEADERS += \
    $$PWD/dispatcher.h \
    $$PWD/trainer.h

SOURCES += \
    $$PWD/dispatcher.cpp \
    $$PWD/trainer.cpp

FORMS += \
    $$PWD/dispatcher.ui
