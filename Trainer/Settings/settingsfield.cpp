#include "settingsfield.h"
#include "ui_settingsfield.h"

SettingsField::SettingsField(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SettingsField)
{
    path = "DEFAULT";
    checkID = -1;
    singleImage = false;
    ui->setupUi(this);
    settings = new Settings;

    setMax();

    mainLayout      = new QGridLayout;
    sampleBox       = new QGroupBox;
    trainingBox     = new QGroupBox;

    QLabel *import  = new QLabel("import Settings");
    QLabel *wL      = new QLabel("set w");
    QLabel *hL      = new QLabel("set h");

    importSettingsB = new QPushButton("import");
    importPos       = new QPushButton("Pos");
    importNeg       = new QPushButton("neg");
    importImg       = new QPushButton("img");
    data = new QPushButton("data");
    vec  = new QPushButton("vec");
    bg   = new QPushButton("bg");

    save = new QPushButton("save");

    choice = new QComboBox;
    choice->addItem("Sample");
    choice->addItem("Training");

    currentChoice("Sample");

    w = new QSpinBox;
    w->setRange(0,300);
    w->setSingleStep(1);
    h = new QSpinBox;
    h->setRange(0,300);
    h->setSingleStep(1);

    choice->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
    importPos->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
    importNeg->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
    importImg->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
    wL->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
    hL->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));

    QHBoxLayout *importLayout       = new QHBoxLayout;
    QHBoxLayout *choiceLayout       = new QHBoxLayout;
    QHBoxLayout *wLay               = new QHBoxLayout;
    QHBoxLayout *hLay               = new QHBoxLayout;
    QFormLayout *formSampleLayout   = new QFormLayout;
    QFormLayout *formTrainingLayout = new QFormLayout;

    formSampleLayout   = drawSample();
    formTrainingLayout = drawTraining();

    importLayout->addWidget(import);
    importLayout->addWidget(importSettingsB);

    choiceLayout->addWidget(importImg);
    choiceLayout->addWidget(importPos);
    choiceLayout->addWidget(importNeg);
    choiceLayout->addWidget(data);
    choiceLayout->addWidget(vec);
    choiceLayout->addWidget(bg);
    choiceLayout->addWidget(choice);

    wLay->addWidget(wL);
    wLay->addWidget(w);

    hLay->addWidget(hL);
    hLay->addWidget(h);

    QGroupBox *imp   = new QGroupBox;
    QGroupBox *cho   = new QGroupBox;
    QGroupBox *wB    = new QGroupBox;
    QGroupBox *hB    = new QGroupBox;

    imp->setContentsMargins(-1, -10, -1, -10);
    cho->setContentsMargins(-1, -10, -1, -10);
    wB->setContentsMargins(-1, -10, -1, -10);
    hB->setContentsMargins(-1, -10, -1, -10);

    sampleBox->setLayout(formSampleLayout);
    trainingBox->setLayout(formTrainingLayout);

    imp->setLayout(importLayout);
    cho->setLayout(choiceLayout);
    wB->setLayout(wLay);
    hB->setLayout(hLay);

    mainLayout->addWidget(imp,0,1);
    mainLayout->addWidget(cho,1,1);
    mainLayout->addWidget(sampleBox,2,1);
    mainLayout->addWidget(trainingBox,3,1);
    mainLayout->addWidget(wB,4,1);
    mainLayout->addWidget(hB,5,1);
    mainLayout->addWidget(save,6,1);

    ui->centralwidget->setLayout(mainLayout);

    doUpdate();

    connect(importSettingsB,SIGNAL(clicked()),this,SLOT(importSettings()));
    connect(ui->actionShowSettings,SIGNAL(triggered()),this,SLOT(showSettings()));
    connect(choice,SIGNAL(currentTextChanged(QString)),this,SLOT(currentChoice(QString)));
    connect(importPos,SIGNAL(clicked()),this,SLOT(doImportPos()));
    connect(importNeg,SIGNAL(clicked()),this,SLOT(doImportNeg()));
    connect(importImg,SIGNAL(clicked()),this,SLOT(selectImg()));
    connect(data,SIGNAL(clicked()),this,SLOT(selectData()));
    connect(vec,SIGNAL(clicked()),this,SLOT(selectVec()));
    connect(bg,SIGNAL(clicked()),this,SLOT(selectbg()));
    connect(save,SIGNAL(clicked()),this,SLOT(saveSettings()));
    connect(invCheck,SIGNAL(clicked()),this,SLOT(doCheck()));
    connect(randinvCheck,SIGNAL(clicked()),this,SLOT(doCheck()));
    connect(ui->actionSave,SIGNAL(triggered()),this,SLOT(saveSettings()));
}

SettingsField::~SettingsField()
{
    delete ui;
}

void SettingsField::importSettings()
{
    path = searchFile("JSon Files(*.json)");
    settings->setSettings(path);
    doUpdate();
}

void SettingsField::showSettings()
{
    QMessageBox::information(
        this,
        tr("Setting List"),
        tr(QString(settings->settings.value("Sample").toString()
                   +"\nInfo               "+settings->settings.value("info").toString()
                   +"\nImg                "+settings->settings.value("img").toString()
                   +"\nVec                "+settings->settings.value("vec").toString()
                   +"\nBG                 "+settings->settings.value("bg").toString()
                   +"\nNum             "+settings->settings.value("num").toString()
                   +"\nBGcolor       "+settings->settings.value("bgcolor").toString()
                   +"\nBGthresh    "+settings->settings.value("bgthresh").toString()
                   +"\ninv                 "+settings->settings.value("inv").toString()
                   +"\nrandinv        "+settings->settings.value("randinv").toString()
                   +"\nmaxidev      "+settings->settings.value("maxidev").toString()
                   +"\nmaxxangle "+settings->settings.value("maxxangle").toString()
                   +"\nmaxyangle "+settings->settings.value("maxyangle").toString()
                   +"\nmaxzangle "+settings->settings.value("maxzangle").toString()
                   +"\nshow            "+settings->settings.value("show").toString()
                   +"\nw                   "+settings->settings.value("w").toString()
                   +"\nh                    "+settings->settings.value("h").toString()
                   +"\n\n" +settings->settings.value("Training").toString()
                   +"\ndata                                  "   +settings->settings.value("data").toString()
                   +"\nvec                                    "   +settings->settings.value("Tvec").toString()
                   +"\nBG                                     "   +settings->settings.value("Tbg").toString()
                   +"\nnumPos                          "   +settings->settings.value("numPos").toString()
                   +"\nnumNeg                         "   +settings->settings.value("numNeg").toString()
                   +"\nnumStages                    "   +settings->settings.value("numStages").toString()
                   +"\nprecalcValBufSize       " + settings->settings.value("precalcValBufSize").toString()
                   +"\nprecalcIdxBufSize       " + settings->settings.value("precalcIdxBufSize").toString()
                   +"\nbaseFormatSave          " + settings->settings.value("baseFormatSave").toString()
                   +"\nnumThreads                   " + settings->settings.value("numThreads").toString()
                   +"\nfeatureType                  " + settings->settings.value("featureType").toString()
                   +"\nbt                                      " + settings->settings.value("bt").toString()
                   +"\nminHitRate                    " + settings->settings.value("minHitRate").toString()
                   +"\nmaxFalseAlarmRate   " + settings->settings.value("maxFalseAlarmRate").toString()
                   +"\nweightTrimRate           " + settings->settings.value("weightTrimRate").toString()
                   +"\nmaxDepth                      " + settings->settings.value("maxDepth").toString()
                   +"\nmaxWeakCount           " + settings->settings.value("maxWeakCount").toString()
                   +"\nmode                               "+ settings->settings.value("mode").toString()
                   ).toStdString().c_str())
                            );
}

void SettingsField::currentChoice(QString ch)
{
    if(ch=="Sample")
    {
        trainingBox->hide();
        sampleBox->show();
    }
    else
    {
        sampleBox->hide();
        trainingBox->show();
    }
}

void SettingsField::doImportPos()
{
    importPosPath = searchFile("Dat Files(*.dat)");
    doUpdate();
}

void SettingsField::doImportNeg()
{
    importNegPath = searchFile("Dat Files(*.dat)");
    bgPath = importNegPath;
    doUpdate();
}

void SettingsField::selectData()
{
    dataPath = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                            QDir::homePath(),
                                                            QFileDialog::ShowDirsOnly
                                                            | QFileDialog::DontResolveSymlinks);
    if(dataPath=="")
        dataPath = "data/";
    doUpdate();
}

void SettingsField::selectbg()
{
    bgPath = searchFile("Dat Files(*.dat)");
    importNegPath = bgPath;
    doUpdate();
}

void SettingsField::selectVec()
{
    vecPath = searchFile("Vec Files(*.vec)");
    doUpdate();
}

void SettingsField::selectImg()
{
    imgPath = searchFile("Img Files(*.jpg *.png *.bmp)");
    doUpdate();
}

void SettingsField::saveSettings()
{
    if(path=="")
        path="settings.json";
    if(bgPath!="")
        settings->settings.setValue("bg",bgPath);
    if(importPosPath!="")
        settings->settings.setValue("info",importPosPath);
    if(vecPath!="")
        settings->settings.setValue("vec",vecPath);
    if(imgPath!="")
        settings->settings.setValue("img",imgPath);
    setMax();
    num->setValue(num->value());
    settings->settings.setValue("num",num->value());
    settings->settings.setValue("bgcolor",bgcolor->value());
    settings->settings.setValue("bgthresh",bgthresh->value());
    if(invCheck->isChecked())
        settings->settings.setValue("inv","-inv");
    else
        settings->settings.setValue("inv","none");
    if(randinvCheck->isChecked())
        settings->settings.setValue("randinv","-randinv");
    else
        settings->settings.setValue("randinv","none");
    settings->settings.setValue("maxidev",maxidev->value());
    settings->settings.setValue("maxxangle",maxxangle->value());
    settings->settings.setValue("maxyangle",maxyangle->value());
    settings->settings.setValue("maxzagnle",maxzangle->value());
    if(showB->isChecked())
        settings->settings.setValue("show","-show");
    else
        settings->settings.setValue("show","none");
    settings->settings.setValue("w",w->value());
    settings->settings.setValue("h",h->value());
    if(dataPath=="") dataPath = "data/";
    settings->settings.setValue("data",dataPath);
    num->setValue(numPos->value());
    numPos->setValue(numPos->value());
    settings->settings.setValue("numPos",numPos->value());
    settings->settings.setValue("numNeg",numNeg->value());
    settings->settings.setValue("numStages",numStages->value());
    settings->settings.setValue("precalcValBufSize",precalcValBufSize->value());
    settings->settings.setValue("precalcIdxBufSize",precalcIdxBufSize->value());
    if(baseFormatSave->isChecked())
        settings->settings.setValue("baseFormatSave","=baseFormatSave");
    else
        settings->settings.setValue("baseFormatSave","none");
    settings->settings.setValue("numThreads",numThreads->value());
    settings->settings.setValue("featureType",featureType->currentText());
    settings->settings.setValue("bt",bt->currentText());
    settings->settings.setValue("minHitRate",minHitRate->value());
    settings->settings.setValue("maxFalseAlarmRate",maxFalseAlarmRate->value());
    settings->settings.setValue("weightTrimRate",weightTrimRate->value());
    settings->settings.setValue("maxDepth",maxDepth->value());
    settings->settings.setValue("maxWeakCount",maxWeakCount->value());
    settings->settings.setValue("mode",mode->currentText());

    settings->setNewSettings(path);
    doUpdate();
}

QString SettingsField::searchFile(QString extension)
{
    QString file = QFileDialog::getOpenFileName(this, tr("Import File"),
                                                    QDir::homePath(),tr(extension.toStdString().c_str()));

    if(file=="")
        file="DEFAULT";
    qDebug() << file;
    return file;
}

QFormLayout* SettingsField::drawSample()
{
    QFormLayout *formSampleLayout   = new QFormLayout;
    bgcolor = new QSpinBox;
    bgcolor->setRange(0,255);
    bgcolor->setSingleStep(1);    
    bgthresh = new QSpinBox;
    bgthresh->setRange(0,255);
    bgthresh->setSingleStep(1);    
    invCheck = new QCheckBox;
    invCheck->setText("set inv");
    randinvCheck = new QCheckBox;
    randinvCheck->setText("set randinv");
    num = new QSpinBox;
    //num->setRange(0,maxPos);
    num->setMinimum(1);
    num->setSingleStep(1);
    maxxangle = new QDoubleSpinBox;
    maxxangle->setRange(0,10);
    maxxangle->setSingleStep(0.1);
    maxyangle = new QDoubleSpinBox;
    maxyangle->setRange(0,10);
    maxyangle->setSingleStep(0.1);
    maxzangle = new QDoubleSpinBox;
    maxzangle->setRange(0,10);
    maxzangle->setSingleStep(0.1);
    maxidev = new QSpinBox;
    maxidev->setRange(0,1000);
    maxidev->setSingleStep(1);
    showB = new QCheckBox;
    showB->setText("show");
    formSampleLayout->addRow("set bg color",bgcolor);
    formSampleLayout->addRow("set bg threshold",bgthresh);
    formSampleLayout->addRow("inv",invCheck);
    formSampleLayout->addRow("randinv",randinvCheck);
    formSampleLayout->addRow("set num count",num);
    formSampleLayout->addRow("set maxxangle",maxxangle);
    formSampleLayout->addRow("set maxyangle",maxyangle);
    formSampleLayout->addRow("set maxzangle",maxzangle);
    formSampleLayout->addRow("set maxidev count",maxidev);
    formSampleLayout->addRow("show",showB);
    return formSampleLayout;
}

QFormLayout* SettingsField::drawTraining()
{
    QFormLayout *formTrainingLayout   = new QFormLayout;
    numPos = new QSpinBox;
    numPos->setRange(0,maxPos);
    numPos->setSingleStep(1);
    numNeg = new QSpinBox;
    numNeg->setRange(0,maxNeg);
    numNeg->setSingleStep(1);
    numStages = new QSpinBox;
    numStages->setRange(1,100);
    numStages->setSingleStep(1);
    precalcValBufSize = new QSpinBox;
    precalcValBufSize->setRange(256,settings->getMem());
    precalcValBufSize->setSingleStep(1);
    precalcIdxBufSize = new QSpinBox;
    precalcIdxBufSize->setRange(256,settings->getMem());
    precalcIdxBufSize->setSingleStep(1);
    baseFormatSave = new QCheckBox;
    baseFormatSave->setText("base Format");
    numThreads = new QSpinBox;
    numThreads->setRange(0,QThread::idealThreadCount());
    numThreads->setSingleStep(1);
    featureType = new QComboBox;
    featureType->addItem("HAAR");
    featureType->addItem("LBP");
    bt = new QComboBox;
    bt->addItem("DAB");
    bt->addItem("RAB");
    bt->addItem("LB");
    bt->addItem("GAB");
    minHitRate = new QDoubleSpinBox;
    minHitRate->setDecimals(3);
    minHitRate->setRange(0,1);
    minHitRate->setSingleStep(0.001);
    maxFalseAlarmRate = new QDoubleSpinBox;
    maxFalseAlarmRate->setDecimals(3);
    maxFalseAlarmRate->setRange(0,1);
    maxFalseAlarmRate->setSingleStep(0.001);
    weightTrimRate = new QDoubleSpinBox;
    weightTrimRate->setDecimals(3);
    weightTrimRate->setRange(0,1);
    weightTrimRate->setSingleStep(0.001);
    maxDepth = new QDoubleSpinBox;
    maxDepth->setRange(0,2);
    maxDepth->setSingleStep(0.1);
    maxWeakCount = new QSpinBox;
    maxWeakCount->setRange(0,1000);
    maxWeakCount->setSingleStep(1);
    mode = new QComboBox;
    mode->addItem("BASIC");
    mode->addItem("ALL");

    formTrainingLayout->addRow("select pos count",numPos);
    formTrainingLayout->addRow("select neg count",numNeg);
    formTrainingLayout->addRow("select stages count",numStages);
    formTrainingLayout->addRow("precalcValBufSize",precalcValBufSize);
    formTrainingLayout->addRow("precalcIdxBufSize",precalcIdxBufSize);
    formTrainingLayout->addRow("base Format Save",baseFormatSave);
    formTrainingLayout->addRow("select thread count",numThreads);
    formTrainingLayout->addRow("select feature type",featureType);
    formTrainingLayout->addRow("select Type of boost",bt);
    formTrainingLayout->addRow("min hit Rate",minHitRate);
    formTrainingLayout->addRow("max False Alarm Rate",maxFalseAlarmRate);
    formTrainingLayout->addRow("weight trim Rate",weightTrimRate);
    formTrainingLayout->addRow("select max Depth",maxDepth);
    formTrainingLayout->addRow("max Weak Count",maxWeakCount);
    formTrainingLayout->addRow("Haar-like feature parameters",mode);

    return formTrainingLayout;
}

void SettingsField::doUpdate()
{
    setMax();
    bgcolor->setValue(settings->settings.value("bgcolor").toInt());
    bgthresh->setValue(settings->settings.value("bgthresh").toInt());
    if(settings->settings.value("inv")!="none")
    {
        invCheck->setChecked(true);
        checkID = 1;
    }
    if(settings->settings.value("randinv")!="none")
    {
        randinvCheck->setChecked(true);
        checkID = 2;
    }
    num->setRange(0,maxPos);
    num->setValue(settings->settings.value("num").toInt());
    maxxangle->setValue(settings->settings.value("maxxangle").toDouble());
    maxyangle->setValue(settings->settings.value("maxyangle").toDouble());
    maxzangle->setValue(settings->settings.value("maxzangle").toDouble());
    maxidev->setValue(settings->settings.value("maxidev").toInt());
    if(settings->settings.value("show")!="none")
        showB->setChecked(true);
    numPos->setRange(0,maxPos);
    numPos->setValue(settings->settings.value("numPos").toInt());
    numNeg->setRange(0,maxNeg);
    numNeg->setValue(settings->settings.value("numNeg").toInt());
    numStages->setValue(settings->settings.value("numStages").toInt());
    precalcValBufSize->setValue(settings->settings.value("precalcValBufSize").toInt());
    precalcIdxBufSize->setValue(settings->settings.value("precalcIdxBufSize").toInt());
    if(settings->settings.value("baseFormatSave")!="none")
        baseFormatSave->setChecked(true);
    numThreads->setValue(settings->settings.value("numThreads").toInt());
    bt->setCurrentIndex(bt->findText(settings->settings.value("bt").toString()));
    featureType->setCurrentIndex(featureType->findText(settings->settings.value("featureType").toString()));
    minHitRate->setValue(settings->settings.value("minHitRate").toDouble());
    maxFalseAlarmRate->setValue(settings->settings.value("maxFalseAlarmRate").toDouble());
    weightTrimRate->setValue(settings->settings.value("weightTrimRate").toDouble());
    maxDepth->setValue(settings->settings.value("maxDepth").toInt());
    maxWeakCount->setValue(settings->settings.value("maxWeakCount").toInt());
    mode->setCurrentIndex(mode->findText(settings->settings.value("mode").toString()));
    w->setValue(settings->settings.value("w").toInt());
    h->setValue(settings->settings.value("h").toInt());
}

void SettingsField::doCheck()
{
    if(!invCheck->isChecked()&&!randinvCheck->isChecked())
        checkID=-1;
    if(checkID==1)
    {
        invCheck->setChecked(false);
        checkID=2;
    }
    else
        if(checkID==2)
        {
            randinvCheck->setChecked(false);
            checkID=1;
        }
        else
            {
                if(invCheck->isChecked())
                    checkID=1;
                if(randinvCheck->isChecked())
                    checkID=2;
            }
}

void SettingsField::setMax()
{
    maxPos = maxNeg = 0;
    QFile file(settings->settings.value("info").toString());
    file.open(QIODevice::ReadOnly);

    QTextStream in(&file);
    while(!in.atEnd())
    {
        QString str = in.readLine();
        maxPos++;
    }
    file.close();
    file.setFileName(settings->settings.value("bg").toString());
    file.open(QIODevice::ReadOnly);
    while(!in.atEnd())
    {
        {
            QString str = in.readLine();
            maxNeg++;
        }
    }
    if (maxPos == 0) maxPos = 1000;
    if (maxNeg == 0) maxNeg = 1000;
    if(singleImage==true)
        maxPos = 10000;
}
