#ifndef SETTINGSFIELD_H
#define SETTINGSFIELD_H

#include <QMainWindow>
#include <QMessageBox>

#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QGroupBox>
#include <QPushButton>
#include <QLabel>
#include <QComboBox>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QCheckBox>

#include <QDebug>
#include <QFileDialog>
#include <QThread>

#include "settings.h"

namespace Ui {
class SettingsField;
}

class SettingsField : public QMainWindow
{
    Q_OBJECT

public:
    explicit SettingsField(QWidget *parent = 0);
    ~SettingsField();
    Settings *settings;
    QString path;
    int maxPos,maxNeg;
    bool singleImage;
    void doUpdate();
private:
    Ui::SettingsField *ui;

    QPushButton *importSettingsB;
    QPushButton *importPos;
    QPushButton *importNeg;
    QPushButton *importImg;
    QPushButton *data;
    QPushButton *vec;
    QPushButton *bg;
    QPushButton *save;
    QGridLayout *mainLayout;

    QSpinBox *bgcolor;
    QSpinBox *bgthresh;
    QCheckBox *invCheck;
    QCheckBox *randinvCheck;
    QSpinBox *num;
    QDoubleSpinBox *maxxangle;
    QDoubleSpinBox *maxyangle;
    QDoubleSpinBox *maxzangle;
    QSpinBox *maxidev;
    QCheckBox *showB;
    QSpinBox *w;
    QSpinBox *h;

    QSpinBox *numPos;
    QSpinBox *numNeg;
    QSpinBox *numStages;
    QSpinBox *precalcValBufSize;
    QSpinBox *precalcIdxBufSize;
    QCheckBox *baseFormatSave;
    QSpinBox *numThreads;
    QComboBox *featureType;
    QComboBox *bt;
    QDoubleSpinBox *minHitRate;
    QDoubleSpinBox *maxFalseAlarmRate;
    QDoubleSpinBox *weightTrimRate;
    QDoubleSpinBox *maxDepth;
    QSpinBox *maxWeakCount;
    QComboBox *mode;

    QGroupBox *sampleBox;
    QGroupBox *trainingBox;

    QComboBox *choice;

    QString vecPath,bgPath,dataPath,imgPath;
    QString importPosPath,importNegPath;

    QFormLayout* drawSample();
    QFormLayout* drawTraining();

    int checkID;
public slots:
    void showSettings();
private slots:
    void importSettings();
    void doImportPos();
    void doImportNeg();
    QString searchFile(QString extension);
    void currentChoice(QString ch);
    void selectData();
    void selectVec();
    void selectbg();
    void selectImg();
    void saveSettings();
    void doCheck();
    void setMax();
};

#endif // SETTINGSFIELD_H
