#include "directoryanalyzer.h"

DirectoryAnalyzer::DirectoryAnalyzer(QObject *parent) : QObject(parent)
{

}

DirectoryAnalyzer::~DirectoryAnalyzer()
{

}

QStringList DirectoryAnalyzer::findFile(QString name,QString path,bool recursive)
{
    QStringList list;
    if(recursive)
    {
        QDirIterator it(path, QStringList() << name, QDir::Files, QDirIterator::Subdirectories);
        while (it.hasNext())
        {
            list << it.next();
        }
    }
    else
    {
        QStringList nameFilter(name);
        QDir directory(path);
        list = directory.entryList(nameFilter);
    }
    return list;
}

bool DirectoryAnalyzer::makeCopy(QString source, QString destination)
{
    QDir sourceDir(source);
    if(!sourceDir.exists())
        return false;
    QDir destDir(destination);
    if(!destDir.exists())
    {
        destDir.mkdir(destination);
    }
    QStringList files = sourceDir.entryList(QDir::Files);
    for(int i = 0; i< files.count(); i++)
    {
        QString srcName = source + "/" + files[i];
        QString destName = destination + "/" + files[i];
        QFile::copy(srcName, destName);
    }
    files.clear();
    files = sourceDir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot);
    for(int i = 0; i< files.count(); i++)
    {
        QString srcName = source + "/" + files[i];
        QString destName = destination + "/" + files[i];
        makeCopy(srcName,destName);
    }
    return true;
}
bool DirectoryAnalyzer::removeDir(const QString &dirName)
{
    bool result = true;
    QDir dir(dirName);

    if (dir.exists(dirName)) {
        Q_FOREACH(QFileInfo info, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst)) {
            if (info.isDir()) {
                result = removeDir(info.absoluteFilePath());
            }
            else {
                result = QFile::remove(info.absoluteFilePath());
            }

            if (!result) {
                return result;
            }
        }
        result = dir.rmdir(dirName);
    }
    return result;
}
