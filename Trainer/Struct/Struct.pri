INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD

HEADERS += \
    $$PWD/directoryanalyzer.h \
    $$PWD/settings.h

SOURCES += \
    $$PWD/directoryanalyzer.cpp \
    $$PWD/settings.cpp
