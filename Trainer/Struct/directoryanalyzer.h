#ifndef DIRECTORYANALYZER_H
#define DIRECTORYANALYZER_H

#include <QObject>

#include <QDirIterator>
#include <QDir>
#include <QStringList>

class DirectoryAnalyzer : public QObject
{
    Q_OBJECT
public:
    explicit DirectoryAnalyzer(QObject *parent = 0);
    ~DirectoryAnalyzer();

    bool makeCopy(QString source, QString destination);
    bool removeDir (const QString & dirName);
    QStringList findFile(QString name,QString path,bool recursive);

signals:

public slots:
};

#endif // DIRECTORYANALYZER_H
