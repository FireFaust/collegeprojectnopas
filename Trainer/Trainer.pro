#-------------------------------------------------
#
# Project created by QtCreator 2015-03-12T10:16:07
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Trainer
TEMPLATE = app

INCLUDEPATH += /usr/include
LIBS += -L/usr/lib
LIBS += -L/usr/local/lib
LIBS += -lopencv_calib3d -lopencv_contrib -lopencv_features2d \
        -lopencv_flann -lopencv_imgproc -lopencv_ml \
        -lopencv_objdetect -lopencv_video -lopencv_highgui -lopencv_core

SOURCES += main.cpp

include(Proc/Proc.pri)
include(Struct/Struct.pri)
include(Settings/Settings.pri)
include(CreateSamples/CreateSamples.pri)
include(TrainCascade/TrainCascade.pri)
